<?php

	class Main_Controller extends CI_Controller {

		public function index () {
			$this->load->view('welcome_message');
		}

		public function array_helper () {
			$this->load->helper("array");
			$this->load->helper("captcha");

			$this->load->library("form_validation");


			$config = array(
        array(
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'required'
        ),
        array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required',
                'errors' => array(
                        'required' => 'You must provide a %s.',
                ),
        ),
        array(
                'field' => 'passconf',
                'label' => 'Password Confirmation',
                'rules' => 'required'
        ),
        array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required'
        )
);

			$this->form_validation->set_rule($config);
		}
	}

?>