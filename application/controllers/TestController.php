<?php 

class TestController extends CI_Controller {

        
        public function index()
        {
                $this->load->helper(array('form', 'url'));

                $this->load->library('form_validation');

                $this->form_validation->set_rules('username', 'Username', 'required');
                $this->form_validation->set_rules('password', 'Password', 'required',
                        array('required' => 'You must provide a %s.')
                );
                $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required');
                $this->form_validation->set_rules('email', 'Email', 'required',
                        array('required' => 'you mousawejawejaweiawejiaw %s')
                );

                if ($this->form_validation->run() == FALSE)
                {
                        $this->load->view('test_folder/myform');
                }
                else
                {
                        $this->load->view('test_folder/formsuccess');
                }
        }

        public function home () {
$this->load->view('test_folder/formsuccess');
        }
}

?>